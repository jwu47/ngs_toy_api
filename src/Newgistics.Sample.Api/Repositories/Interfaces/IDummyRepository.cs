﻿using Newgistics.Lib.ServiceHost.Dto;
using System.Threading.Tasks;

namespace Newgistics.ToyApi.Service.Repositories.Interfaces
{
    public interface IDummyRepository
    {
        Task<BooleanResult> GetDummy(string id);
    }
}
