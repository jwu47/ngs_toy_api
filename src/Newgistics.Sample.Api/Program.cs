﻿using Newgistics.Lib.ServiceHost.Helper;
using Microsoft.AspNetCore.Hosting;

namespace Newgistics.ToyApi.Service
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var webHost = WebHostHelper.GetHost(typeof(Startup), args);
            webHost.Run();
        }
    }
}
