#!/bin/bash
set -x
BUILD_CONFIG="Release"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

dotnet restore `ls $DIR/*.sln`

for d in src/*/ ; do
  SRC_DIR=$DIR/$d
  dotnet build $SRC_DIR -c $BUILD_CONFIG
  PUB_DIR=${SRC_DIR}bin/publish
  dotnet publish $SRC_DIR -c $BUILD_CONFIG -o ${SRC_DIR}bin/publish
  rtn=$?
  if [ "$rtn" != "0" ]; then 
    exit $rtn
  fi
done

for t in test/*.Unit/ ; do
  TEST_DIR=$DIR/$t
  cd $TEST_DIR
  dotnet test -c $BUILD_CONFIG
  rtn=$?
  if [ "$rtn" != "0" ]; then 
    exit $rtn
  fi
done
